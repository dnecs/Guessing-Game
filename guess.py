""" Simple project testing functionality """
import random

# generate a random interger into variable
random_number = random.randint(1,2020)

# guess the number
my_guess = int(input("Guess a number: "))

# check if your input is same as randomly generated number
if my_guess == random_number:
    print("Good Guess!! the number is ", random_number)
else:
    print("Wrong Answer!! The correct number is:", random_number)